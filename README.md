# Field Translate plugin for Craft CMS 3.x

Translate field contents with google translate API

![Screenshot](resources/img/plugin-logo.png)

## Dev Environment

```bash
 sudo service apache2 restart
 sudo /etc/init.d/mysql start
```

## Installation

To install the plugin, follow these instructions.

1.  Open your terminal and go to your Craft project:

        cd /path/to/project

2.  Then tell Composer to load the plugin:
    `composer require neesh/field-translate`
    If the package lives in a private project it is necessary to use a private access token (in case of gitlab) for the repository.

- First add the repository to the composer.json, example:

  ```
  "repositories": {
    "gitlab.com/9282356": {
    "type": "composer",
    "url": "https://gitlab.com/api/v4/group/9282356/-/packages/composer/packages.json"
    }
  }
  ```

- Add an auth.json file with the private access token that has the scope "read_api":

  ````
   {
   "gitlab-token": {
           "gitlab.com": "mysecrettoken"
   }
   }```
  ````

- require the package
  `composer req neesh/field-translate:dev-master`
  more info: https://docs.gitlab.com/ee/user/packages/composer_repository/index.html

3.  In the Control Panel, go to Settings → Plugins and click the “Install” button for Field Translate.

## Field Translate Overview

Translate CraftCMS Entries/Categories with Google Translate API

## Configuring Field Translate

-Insert text here-

## Using Field Translate

-Insert text here-

## Field Translate Roadmap

Some things to do, and ideas for potential features:

- Release it

Brought to you by [neeshstudio](https://neesh.de)
