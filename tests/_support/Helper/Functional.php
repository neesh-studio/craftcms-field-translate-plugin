<?php
/**
 * Field Translate plugin for Craft CMS 3.x
 *
 * Translate field contents with google translate API
 *
 * @link      https://neesh.de
 * @copyright Copyright (c) 2021 neeshstudio
 */

namespace Helper;

use Codeception\Module;

/**
 * Class Functional
 *
 * Here you can define custom actions.
 * All public methods declared in helper class will be available in $I
 */
class Functional extends Module
{

}
