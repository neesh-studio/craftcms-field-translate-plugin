function validateSelection() {
  let checkboxes = document.getElementsByName("entryIds");
  let numberOfCheckedItems = 0;
  for (var i = 0; i < checkboxes.length; i++) {
    if (checkboxes[i].checked) numberOfCheckedItems++;
  }
  if (numberOfCheckedItems < 1) {
    alert("you need to select at least one");
    return false;
  }
}
