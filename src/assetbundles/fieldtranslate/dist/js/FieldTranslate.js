/**
 * Field Translate plugin for Craft CMS
 *
 * Field Translate JS
 *
 * @author    neeshstudio
 * @copyright Copyright (c) 2021 neeshstudio
 * @link      https://neesh.de
 * @package   FieldTranslate
 * @since     0.1.0
 */
