<?php

/**
 * Field Translate plugin for Craft CMS 3.x
 *
 * Translate field contents with google translate API
 *
 * @link      https://neesh.de
 * @copyright Copyright (c) 2021 neeshstudio
 */

namespace neesh\fieldtranslate\controllers;

use neesh\fieldtranslate\FieldTranslate;
use neesh\fieldtranslate\models\TranslationEntryModel;

use Google\Cloud\Translate\V3\TranslationServiceClient;
use Google\Cloud\Translate\V3\Glossary;
use Google\Cloud\Translate\V3\GlossaryInputConfig;
use Google\Cloud\Translate\V3\TranslateTextGlossaryConfig;
use Google\Cloud\Translate\V3\GcsSource;
use Google\Cloud\Translate\V3\Glossary\LanguageCodesSet;


use Craft;
use craft\elements\Entry;
use craft\elements\Category;

use craft\elements\Field;
use craft\web\Controller;
use craft\web\View;
use verbb\supertable\SuperTable;
use yii\web\Response;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    neeshstudio
 * @package   FieldTranslate
 * @since     0.1.0
 */
class DefaultController extends Controller
{

    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something'];



    // Public Methods
    // =========================================================================

    /**
     * @param array $context
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig_Error_Loader
     * @throws \yii\base\Exception
     */
    public function renderEditSourceLink(array $context)
    {
        $entry = $context['entry'] ?? null;
        if ($entry) {
            return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-entry-type-link', $context);
        }
        // $asset = ($context['element'] ?? null) && $context['element'] instanceof Asset ? $context['element'] : null;
        // if ($asset) {
        //     $context['asset'] = $context['element'];
        //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-volume-link', $context);
        // }
        // $globalSet = $context['globalSet'] ?? null;
        // if ($globalSet) {
        //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-globalset-link', $context);
        // }
        // $user = $context['user'] ?? null;
        // if ($user) {
        //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-users-link', $context);
        // }
        // $category = $context['category'] ?? null;
        // if ($category) {
        //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-category-group-link', $context);
        // }
        // $product = $context['product'] ?? null;
        // if ($product) {
        //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-commerce-product-type-link', $context);
        // }
        return '';
    }




    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/field-translate/default
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $result = 'Welcome to the DefaultController actionIndex() method';

        return $result;
    }


    public function actionDoSomething()
    {
        $result = 'Welcome to the DefaultController actionDoSomething() method';

        return $result;
    }





    /**
     * e.g.: actions/field-translate/default/translate-entry
     * http://localhost/admin/field-translate/request-translations?targetLanguage=en&sourceSite=default&targetSite=localDevDe&entryIds[]=6&entryIds[]=3
     * @return mixed
     */
    public function actionTranslateEntry($sourceSite, array $targetSites = [], array $entryIds = [], array $categoryIds = [], array $assetIds = [], $includeGlobals = false)
    {
        $this->requireLogin();
        $this->requireCpRequest();

        $resultArray = array('errors' => [], 'translationResults' => []);
        foreach ($targetSites as $targetSite) {
            $result = FieldTranslate::getInstance()->translate->translateElements($sourceSite, $targetSite, $entryIds, $categoryIds, $assetIds, $includeGlobals);
            $resultArray['errors'] = array_merge($resultArray['errors'], $result['errors']);
            $resultArray['translationResults'] = array_merge($resultArray['translationResults'], $result['translationResults']);
        }
        return $this->renderTemplate(
            'field-translate/translate-result',
            [
                'errors' => $resultArray['errors'],
                'translationResults' => $resultArray['translationResults'],
                // 'glossaries' => $glossaryList,
            ],
            View::TEMPLATE_MODE_CP,
        );
    }

    public function actionSaveGlossary()
    {
        $this->requireLogin();
        $glossary = Craft::$app->request->getParam('glossary');
        $header_map = function ($arg) {
            return $arg['heading'];
        };
        $rawCols = Craft::$app->request->getParam('cols');
        $cols = array_map($header_map, json_decode($rawCols, true));
        // var_dump($cols);
        FieldTranslate::getInstance()->translate->saveGlossary($cols, $glossary);
        // var_dump($glossary);
    }

    public function actionCreateGlossary()
    {
        $this->requireAdmin();
        FieldTranslate::getInstance()->translate->createGlossary();
    }
}
