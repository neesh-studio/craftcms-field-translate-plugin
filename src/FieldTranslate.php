<?php

/**
 * Field Translate plugin for Craft CMS 3.x
 *
 * Translate field contents with google translate API
 *
 * @link      https://neesh.de
 * @copyright Copyright (c) 2021 neeshstudio
 */

namespace neesh\fieldtranslate;

use neesh\fieldtranslate\models\Settings;
use neesh\fieldtranslate\controllers\DefaultController;

use Craft;
use craft\base\Plugin;
use craft\elements\Asset;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\UrlManager;
use craft\events\RegisterUrlRulesEvent;
use craft\helpers\FileHelper;
use craft\helpers\Html;
use neesh\fieldtranslate\FieldTranslate as FieldtranslateFieldTranslate;
use yii\base\Event;
use League\Csv\Reader;
use craft\events\RegisterCpNavItemsEvent;
use craft\web\twig\variables\Cp;
use craft\web\twig\variables\CraftVariable;
use neesh\fieldtranslate\services\TranslateService;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://docs.craftcms.com/v3/extend/
 *
 * @author    neeshstudio
 * @package   FieldTranslate
 * @since     0.1.0
 *
 * @property  Settings $settings
 * @method    Settings getSettings()
 */
class FieldTranslate extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * FieldTranslate::$plugin
     *
     * @var FieldTranslate
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * To execute your plugin’s migrations, you’ll need to increase its schema version.
     *
     * @var string
     */
    public $schemaVersion = '0.1.0';

    /**
     * Set to `true` if the plugin should have a settings view in the control panel.
     *
     * @var bool
     */
    public $hasCpSettings = true;

    /**
     * Set to `true` if the plugin should have its own section (main nav item) in the control panel.
     *
     * @var bool
     */
    public $hasCpSection = true;

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * FieldTranslate::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents([
            'translate' => \neesh\fieldtranslate\services\TranslateService::class,
        ]);

        // // Register our site routes
        // Event::on(
        //     UrlManager::class,
        //     UrlManager::EVENT_REGISTER_SITE_URL_RULES,
        //     function (RegisterUrlRulesEvent $event) {
        //         $event->rules['siteActionTrigger1'] = 'field-translate/default';
        //     }
        // );

        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_LOAD_PLUGINS,
            function () {
                $this->setupEditPreviews();
            }
        );


        // Register our CP routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_CP_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules = array_merge($event->rules, [
                    'cpActionTrigger1' => 'field-translate/default/do-something',
                    'field-translate/request-translations' => 'field-translate/default/translate-entry',
                    'field-translate/save-glossary' => 'field-translate/default/save-glossary',
                    'field-translate/create-glossary' => 'field-translate/default/create-glossary'
                ]);
                // $event->rules['cpActionTrigger1'] = 'field-translate/default/do-something';
            }
        );

        Event::on(CraftVariable::class, CraftVariable::EVENT_INIT, function (Event $e) {

            $translateService = $e->sender;
            // Attach a service:
            $translateService->set('translate', TranslateService::class);
        });



        // Event::on(
        //     Cp::class,
        //     Cp::EVENT_REGISTER_CP_NAV_ITEMS,
        //     function (RegisterCpNavItemsEvent $event) {
        //         $event->navItems[] = [
        //             'url' => 'field-translate',
        //             'label' => 'Field Translate',
        //             'subnav' => [
        //                 'translate' => ['label' => 'Translate Entries', 'url' => 'field-translate'],
        //                 'glossary' => ['label' => 'Glossary', 'url' => 'field-translate/glossary'],


        //             ],


        //         ];
        //     }
        // );


        // Do something after we're installed
        Event::on(
            Plugins::class,
            Plugins::EVENT_AFTER_INSTALL_PLUGIN,
            function (PluginEvent $event) {
                if ($event->plugin === $this) {
                    // We were just installed
                }
            }
        );

        /**
         * Logging in Craft involves using one of the following methods:
         *
         * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
         * Craft::info(): record a message that conveys some useful information.
         * Craft::warning(): record a warning message that indicates something unexpected has happened.
         * Craft::error(): record a fatal error that should be investigated as soon as possible.
         *
         * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
         *
         * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
         * the category to the method (prefixed with the fully qualified class name) where the constant appears.
         *
         * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
         * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
         *
         * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
         */
        Craft::info(
            Craft::t(
                'field-translate',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    public function getCpNavItem()
    {
        $item = parent::getCpNavItem();
       
        $item['subnav'] = [ 
            'translate' => ['label' => 'Translate Entries', 'url' => 'field-translate'],
            'glossary' => ['label' => 'Glossary', 'url' => 'field-translate/glossary'],
        ];
        return $item;
    }


    /**
     * @param array $context
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig_Error_Loader
     * @throws \yii\base\Exception
     */
    // public function renderEditSourceLink(array $context)
    // {
    //     $entry = $context['entry'] ?? null;
    //     if ($entry) {
    //         return Craft::$app->getView()->renderTemplate('field-translate/translate-button', $context);
    //     }


    //     // $asset = ($context['element'] ?? null) && $context['element'] instanceof Asset ? $context['element'] : null;
    //     // if ($asset) {
    //     //     $context['asset'] = $context['element'];
    //     //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-volume-link', $context);
    //     // }
    //     // $globalSet = $context['globalSet'] ?? null;
    //     // if ($globalSet) {
    //     //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-globalset-link', $context);
    //     // }
    //     // $user = $context['user'] ?? null;
    //     // if ($user) {
    //     //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-users-link', $context);
    //     // }
    //     // $category = $context['category'] ?? null;
    //     // if ($category) {
    //     //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-category-group-link', $context);
    //     // }
    //     // $product = $context['product'] ?? null;
    //     // if ($product) {
    //     //     return Craft::$app->getView()->renderTemplate('cp-field-inspect/edit-commerce-product-type-link', $context);
    //     // }
    //     return '';
    // }

    // Protected Methods
    // =========================================================================

    /**
     * Creates and returns the model used to store the plugin’s settings.
     *
     * @return \craft\base\Model|null
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }



    /**
     * Returns the rendered settings HTML, which will be inserted into the content
     * block on the settings page.
     *
     * @return string The rendered settings HTML
     */
    protected function settingsHtml(): string
    {

        // $settings = $this->getSettings();
        // $settings->selectedGlossary = Asset::find()
        //     ->id($settings->selectedGlossary)
        //     ->status(null)
        //     ->enabledForSite(false)
        //     ->all();
        // $glossaryList = FieldTranslate::getInstance()->translate->listGlossaries();



        return Craft::$app->view->renderTemplate(
            'field-translate/settings',
            [
                'settings' => $this->getSettings(),

                // 'glossaries' => $glossaryList,
            ]
        );
    }

    protected function setupEditPreviews()
    {
        $view = Craft::$app->getView();
        // $view->hook('cp.assets.edit.meta', [$this, 'renderEditSourceLink']);
        // $view->hook('cp.entries.edit.meta', [$this, 'renderEditSourceLink']);
        // $view->hook('cp.volumes.edit.settings', [$this, 'renderEditSourceLink']);
    }
}
