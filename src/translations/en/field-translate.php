<?php
/**
 * Field Translate plugin for Craft CMS 3.x
 *
 * Translate field contents with google translate API
 *
 * @link      https://neesh.de
 * @copyright Copyright (c) 2021 neeshstudio
 */

/**
 * Field Translate en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('field-translate', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    neeshstudio
 * @package   FieldTranslate
 * @since     0.1.0
 */
return [
    'Field Translate plugin loaded' => 'Field Translate plugin loaded',
];
