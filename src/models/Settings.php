<?php

/**
 * Field Translate plugin for Craft CMS 3.x
 *
 * Translate field contents with google translate API
 *
 * @link      https://neesh.de
 * @copyright Copyright (c) 2021 neeshstudio
 */

namespace neesh\fieldtranslate\models;

use neesh\fieldtranslate\FieldTranslate;

use Craft;
use craft\base\Model;

/**
 * FieldTranslate Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    neeshstudio
 * @package   FieldTranslate
 * @since     0.1.0
 */
class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * Ignore Letter Casing in Glossary
     *
     * @var string
     */
    public $ignoreCase = '';

    /**
     * Enable/Disable Glossary usage
     *
     * @var string
     */
    public $useGlossary = '';

    /**
     * Google Translate Keys
     *
     * @var string
     */
    public $gtKeys = "";


    /**
     * Glossary Id
     * 
     * @var string
     * 
     */
    public $glossaryId = 'dko_v3';


    /**
     * Glossary Content (Table)
     * 
     * @var string
     * 
     */
    public $glossaryContent;


    /**
     * Glossary File
     * 
     * 
     * 
     */
    public $selectedGlossary;

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
           
            ['gtKeys', 'string'],
            ['gtKeys', 'default', 'value' => ""],
            ['gtKeys', function ($attribute, $params, $validator) {
                if (is_null(json_decode($this->$attribute))) {
                    $this->addError($attribute, 'The gtKeys must be a valid json object.');
                }
            }],
            ['glossaryId', 'string'],
        ];
    }
}
