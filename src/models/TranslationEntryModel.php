<?php

/**
 * Field Translate plugin for Craft CMS 3.x
 *
 * Translate field contents with google translate API
 *
 * @link      https://neesh.de
 * @copyright Copyright (c) 2021 neeshstudio
 */

namespace neesh\fieldtranslate\models;

use neesh\fieldtranslate\FieldTranslate;
use Google\Cloud\Translate\V3\Translation;

use Craft;
use craft\base\Model;

/**
 * FieldTranslateModel Model
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    neeshstudio
 * @package   FieldTranslate
 * @since     0.1.0
 */
class TranslationEntryModel extends Model
{
    // Public Properties
    // =========================================================================


    /**
     * Field type
     *
     * @var string
     */
    public $fieldType;

    /**
     * Entry type
     *
     * @var string
     */
    public $entryType;

    /**
     * Entry Id
     *
     * @var string
     */
    public $entryId;

    /**
     * Field Handle
     *
     * @var string
     */
    public $fieldHandle;

    /**
     * Parent Entry (only set if the entry is a matrix or similar )
     * 
     * @var TranslationEntryModel
     */

    public $parent;

    /**
     * Field Layout Id
     *
     * @var string
     */
    public $fieldLayoutId;


    /**
     * The original (to be translated) text
     *
     * @var string
     */
    public $sourceText = '';

    /**
     * The translated text
     *
     * @var string
     */
    public $translation;



    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['someAttribute', 'string'],
            ['someAttribute', 'default', 'value' => ''],
        ];
    }
}
