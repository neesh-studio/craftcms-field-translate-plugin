<?php

namespace neesh\fieldtranslate\services;

use neesh\fieldtranslate\models\TranslationEntryModel;

use Google\Cloud\Translate\V3\TranslationServiceClient;
use Google\Cloud\Translate\V3\Glossary;
use Google\Cloud\Translate\V3\GlossaryInputConfig;
use Google\Cloud\Translate\V3\TranslateTextGlossaryConfig;
use Google\Cloud\Translate\V3\GcsSource;
use Google\Cloud\Translate\V3\Glossary\LanguageCodesSet;

use League\Csv\Reader;
use League\Csv\Writer;


use Craft;
use craft\elements\Entry;
use craft\elements\Category;
use craft\elements\Asset;
use craft\helpers\FileHelper;
use craft\helpers\Html;

use craft\elements\Field;
use craft\helpers\Assets;
use craft\elements\GlobalSet;
use craft\web\Controller;
use Throwable;
use verbb\supertable\SuperTable;

use yii\base\Component;

class TranslateService extends Component
{

    protected function map_headers($entry): array
    {
        return array('heading' => $entry, 'type' => 'text');
    }

    /**
     * Returns the rendered settings HTML, which will be inserted into the content
     * block on the settings page.
     *
     * @return League/Csv/Reader The rendered settings HTML
     */

    public function getGlossaryContent()
    {
        $localCopy = Asset::findOne(['filename' => 'dko_glossar.csv'])->getCopyOfFile();
        // $contents = Html::encode(file_get_contents($localCopy));
        $contents = file_get_contents($localCopy);

        $csv = Reader::createFromString($contents);
        $csv->setHeaderOffset(0);

        // $header = $csv->getHeader(); //returns the CSV header record
        // $records = $csv->getRecords(); //returns all the CSV records as an Iterator object
        // var_dump(iterator_to_array($csv->getRecords()));

        FileHelper::unlink($localCopy);

        $rawRecords = $csv->getRecords();
        $mappedRecords = array();

        foreach ($rawRecords as $offset => $data) {

            $newArray = array();
            for ($i = 1; $i <= sizeof($data); $i++) {
                // $newArray["col$i"] = array_values($data)[$i - 1];
                array_push($newArray, array_values($data)[$i - 1]);
            }

            array_push($mappedRecords, $newArray);
        }
        $headers = array_map(array($this, 'map_headers'), $csv->getHeader());
        return array('headers' => $headers, 'rows' => $mappedRecords);
    }

    public function saveGlossary(array $cols, array $glossary)
    {
        $asset = Asset::findOne(['filename' => 'dko_glossar.csv']);
        $localCopy = $asset->getCopyOfFile();
        $contents = Html::encode(file_get_contents($localCopy));
        $oldCsv = Reader::createFromString($contents);
        $oldCsv->setHeaderOffset(0);
        $header = $oldCsv->getHeader();

        $tempFilePath = Assets::tempFilePath('csv');

        $writer = Writer::createFromPath($tempFilePath, 'w+');
        $writer->setDelimiter(',');
        $writer->insertOne($cols);
        $writer->insertAll($glossary); //using an array

        // var_dump($tempFilePath);
        // file_put_contents($tempFilePath, $sFileContents);
        Craft::$app->assets->replaceAssetFile($asset, $tempFilePath, 'dko_glossar.csv');

        $success = Craft::$app->elements->saveElement($asset);

        $this->createGlossary();            // recreate glossary to update it, wtf google
    }

    public function translateElements($sourceSite, $targetSite, array $entryIds = [], array $categoryIds = [], array $assetIds = [], $includeGlobals)
    {

        // var_dump($assetIds);
        // return array('errors' => array("exit early"), "translationResults" => array());
        $errors = [];
        $pluginSettings =  Craft::$app->plugins->getPlugin('field-translate')->getSettings();
        $gtKeys = json_decode($pluginSettings->gtKeys, true);

        $targetLanguage = explode("-", Craft::$app->sites->getSiteByHandle($targetSite)->language)[0];       //FIXME this is now only taking the part of thelanguage code before a "-", so pretty unspecific 
        $sourceLanguageCode = Craft::$app->sites->getSiteByHandle($sourceSite)->language;

        // Get all relevant entries from the default site (in this case default=de)
        $entries = Entry::find()
            ->id($entryIds)
            // ->section('translatableSection')
            ->site($sourceSite)
            ->orderBy('postDate asc')
            ->all();


        $categories = Category::find()
            ->id($categoryIds)
            ->site($sourceSite)
            ->orderBy('title asc')
            ->all();

        $assets = Asset::find()
            ->id($assetIds)
            ->site($sourceSite)
            ->orderBy('title asc')
            ->all();


        $globals = GlobalSet::find()->site($sourceSite)->all();
        // var_dump($globals);

        // read all translatable fields of the relavant entries and store them in the TEM data structure
        $fieldsToTranslate = array();
        foreach ($entries as $entry) {
            // array_push($fieldsToTranslate, $entry->title);
            $fieldsToTranslate = array_merge($fieldsToTranslate, $this->readInEntry($entry));
        }
        foreach ($categories as $entry) {
            // array_push($fieldsToTranslate, $entry->title);
            $fieldsToTranslate = array_merge($fieldsToTranslate, $this->readInEntry($entry));
        }
        foreach ($assets as $entry) {
            $fieldsToTranslate = array_merge($fieldsToTranslate, $this->readInEntry($entry));
        }
        if ($includeGlobals == "on") {

            foreach ($globals as $global) {
                $entry = $this->readInEntry($global);
                // var_dump($entry);

                $fieldsToTranslate = array_merge($fieldsToTranslate, $entry);
            }
        }

        // map TEM array to consist only of a string array that google api likes
        $mapFunc = function ($a) {
            return $a->sourceText;
        };

        $googleTranslateArray = array_map($mapFunc, $fieldsToTranslate);
        // var_dump($googleTranslateArray);

        // TODO split googleTranslateArray into multiple arrays that each have a maximum of 30k chars

        $totalLength = 0;
        $packagedTranslateArrays = array(array());

        foreach ($googleTranslateArray as $stringToTranslate) {
            $length = strlen(utf8_decode($stringToTranslate));

            if ($length > 30000) {
                // the single string that we are trying to translate is already greater than the max value
                return array('errors' => array("There is one field that is too long to be translated, please try to split the text in multiple content blocks.", $stringToTranslate), "translationResults" => array());
            }
            if (($length + $totalLength) > 30000) {
                // we need to split here
                array_push($packagedTranslateArrays, array());
                $totalLength = 0;
            }

            array_push($packagedTranslateArrays[count($packagedTranslateArrays) - 1], $stringToTranslate);
            $totalLength += $length;

            if ($totalLength > 100000) {
                return array('errors' => array("You supplied to many items at once, please try to select less entries at once"), "translationResults" => array());
            }
        }

        // putenv('GOOGLE_APPLICATION_CREDENTIALS=/var/www/html/craftcms/vendor/neesh/field-translate/src/keys.json');

        $translationClient = new TranslationServiceClient(array("credentials" => $gtKeys));
        $translations = array();

        try {
            // Send String Array to Google for translation

            if ($pluginSettings->useGlossary == '1') {
                $glossaryConfig = new TranslateTextGlossaryConfig();
                $glossaryPath = "projects/" . $gtKeys['project_id'] . "/locations/us-central1/glossaries/" . $pluginSettings->glossaryId;
                $glossaryConfig->setGlossary($glossaryPath);
                $glossaryConfig->setIgnoreCase($pluginSettings->ignoreCase == '1');
                // var_dump($glossaryConfig);
                foreach ($packagedTranslateArrays as $packagedArray) {
                    $response = $translationClient->translateText(
                        $packagedArray,
                        $targetLanguage,
                        TranslationServiceClient::locationName($gtKeys['project_id'], 'us-central1'),
                        array("glossaryConfig" => $glossaryConfig, "sourceLanguageCode" => $sourceLanguageCode)
                    );

                    // TODO error handling if $response has errors

                    // Data wrangling to ease dealing with the returned data

                    foreach ($response->getGlossaryTranslations() as $translation) {
                        // echo $translation->getTranslatedText() . " : ";
                        array_push($translations, $translation);
                    }
                }
            } else {
                $response = $translationClient->translateText(
                    $googleTranslateArray,
                    $targetLanguage,
                    TranslationServiceClient::locationName($gtKeys['project_id'], 'us-central1'),
                    array("sourceLanguageCode" => $sourceLanguageCode)
                );

                foreach ($response->getTranslations() as $translation) {
                    // echo $translation->getTranslatedText() . " : ";
                    array_push($translations, $translation);
                }
            }
        } catch (Throwable $t) {
            array_push($errors, $t->getMessage());
            Craft::error($t, "field-translate");
        } finally {
            $translationClient->close();
        }

        try {
            $populateTranslation = function ($tem, $translation) {
                $tem->translation = $translation->getTranslatedText();
                return $tem;
            };

            $translatedEntries = array_map($populateTranslation, $fieldsToTranslate, $translations);
        } catch (Throwable $t) {
            array_push($errors, $t->getMessage());
            Craft::error($t, "field-translate");
        }

        try {
            // Actually update/set the translations within Craft
            $previousTarget = null;
            $previousParent = null;
            foreach ($translatedEntries as $tem) {

                $targetElement = $this->getElement($targetSite, $tem->entryType, $tem->entryId, $tem->parent);

                if ($targetElement) {
                    $targetAlreadyModified = !is_null($previousTarget) && $targetElement->id == $previousTarget->id;        // TODO maybe this also needs to check if the targetSite matches
                    if ($targetAlreadyModified) {
                        $targetElement = $previousTarget;
                    }
                    $this->updateField($tem, $targetElement);
                    if (!$targetAlreadyModified && !is_null($previousTarget)) {
                        Craft::$app->elements->saveElement($previousTarget);
                    }
                } else {
                    $msg = "could not update $targetSite, $tem->entryType, $tem->entryId, $tem->fieldHandle, $tem->fieldType.";
                    Craft::warning($msg, 'field-translate');
                    array_push($errors, $msg);
                }

                if ($tem->parent) {
                    $parentElement = $this->getElement($targetSite, $tem->parent->entryType, $tem->parent->entryId, $tem->parent->parent);
                    $parentAlreadyModified = !is_null($previousParent) && $previousParent->id == $parentElement->id;            // TODO maybe this also needs to check if the targetSite matches
                    if (!$parentAlreadyModified && !is_null($previousParent)) {
                        Craft::$app->elements->saveElement($parentElement);
                    }
                    $previousParent = $parentElement;
                }
                $previousTarget = $targetElement;
            }
            // Make sure that the last element gets saved if the loop ends
            if (!is_null($previousTarget)) {
                Craft::$app->elements->saveElement($previousTarget);
            }
            if (!is_null($previousParent)) {
                Craft::$app->elements->saveElement($previousParent);
            }
        } catch (Throwable $t) {
            array_push($errors, $t->getMessage());
            Craft::error($t, "field-translate");
        }

        $flatten_translation = function ($t) {
            return $t->getTranslatedText();
        };
        $translationArray = array_map($flatten_translation, $translations);

        $everythingIsEmpty = true;
        $translationResultArray = array();
        try {
            for ($index = 0; $index < sizeof($googleTranslateArray); $index++) {
                if ($translationArray[$index] != '') {
                    $everythingIsEmpty = false;
                }
                array_push($translationResultArray, [$googleTranslateArray[$index], $translationArray[$index]]);
            }
        } catch (Throwable $t) {
            array_push($errors, $t->getMessage());
            Craft::error($t, "field-translate");
        }

        if ($everythingIsEmpty == true) {
            array_push($errors, 'All Translations came back empty. It might be that you have to select a different source language in order to be able to translate into this language. Google Translate usually works best with \'English\' as a source Language');
        }
        return array('errors' => $errors, "translationResults" => $translationResultArray);
    }

    public function listGlossaries()
    {
        $pluginSettings =  Craft::$app->plugins->getPlugin('field-translate')->getSettings();
        $gtKeys = json_decode($pluginSettings->gtKeys, true);
        $translationServiceClient = new TranslationServiceClient(array("credentials" => $gtKeys));

        $list = array();

        try {
            $formattedParent = $translationServiceClient->locationName($gtKeys['project_id'], 'us-central1');
            // Iterate over pages of elements
            $pagedResponse = $translationServiceClient->listGlossaries($formattedParent);
            foreach ($pagedResponse->iteratePages() as $page) {
                foreach ($page as $element) {

                    array_push($list, $element);
                }
            }
        } finally {
            $translationServiceClient->close();
        }

        return $list;
    }

    protected function getTranslationModel($entry, $field, $parent)
    {
        $tem = new TranslationEntryModel();
        $tem->fieldLayoutId = $field->layoutId;
        $tem->fieldHandle = $field->handle;
        $tem->entryId = $entry->id;
        $tem->entryType = $entry->className();
        $tem->fieldType = $field->className();
        $tem->sourceText = strval($entry->getFieldValue($field->handle));
        $tem->parent = $parent;
        return $tem;
    }

    protected function readInEntry($entry, $parent = null)
    {
        $fieldsToTranslate = array();

        if ($entry->isTitleTranslatable === true && !$parent && $entry->className() != "craft\\elements\GlobalSet") {
            $titleTem = new TranslationEntryModel();
            $titleTem->sourceText = $entry->title;
            $titleTem->entryId = $entry->id;
            $titleTem->fieldType = "titleField";
            $titleTem->entryType = $entry->className();
            array_push($fieldsToTranslate, $titleTem);
            // var_dump($titleTem);
        }

        foreach ($entry->getFieldLayout()->getFields() as $fieldLayoutField) {
            // var_dump($fieldLayoutField->translationMethod);
            if ($fieldLayoutField->translationMethod == "language") {
                switch ($fieldLayoutField->className()) {
                    case "craft\\fields\\PlainText":
                    case "craft\\redactor\\Field":
                        // $tem = $this->getTranslationModel($entry, $fieldLayoutField);
                        $tem = $this->getTranslationModel($entry, $fieldLayoutField, $parent);
                        if (trim($tem->sourceText) !== "" && !is_null($tem->sourceText)) array_push($fieldsToTranslate, $tem);
                        break;
                        // case "craft\\fields\\Table":
                        //     // var_dump(json_encode($entry->getFieldValue($fieldLayoutField->handle)));
                        //     break;
                    default:
                        Craft::warning("\nWarning, type not handled: " . $fieldLayoutField->className(), 'field-translate');
                        // var_dump($entry->getFieldValue($fieldLayoutField->handle));
                }
                // var_dump($entry->getFieldValue($fieldLayoutField->handle));

            } else if ($fieldLayoutField->className() == "craft\\fields\\Matrix") {
                $matrixBlocks = $entry->getFieldValue($fieldLayoutField->handle)->all();
                foreach ($matrixBlocks as $block) {
                    $tem = $this->getTranslationModel($entry, $fieldLayoutField, $parent);
                    $fieldsToTranslate = array_merge($fieldsToTranslate, $this->readInEntry($block, $tem));
                }
            } else if ($fieldLayoutField->className() == "verbb\\supertable\\fields\\SuperTableField") {
                $matrixBlocks = $entry->getFieldValue($fieldLayoutField->handle)->all();
                foreach ($matrixBlocks as $block) {
                    $tem = $this->getTranslationModel($entry, $fieldLayoutField, $parent);

                    $fieldsToTranslate = array_merge($fieldsToTranslate, $this->readInEntry($block, $tem));
                }
            }
        }
        return $fieldsToTranslate;
    }

    protected function updateField($tem, $entry)
    {
        switch ($tem->fieldType) {
            case "titleField":
                $entry->title = $tem->translation;
                // Craft::$app->elements->saveElement($entry);
                break;
            case "craft\\fields\\PlainText":
            case "craft\\redactor\\Field":
                if ($entry && $entry->id == $tem->entryId) {
                    $entry->setFieldValue($tem->fieldHandle, $tem->translation);
                    // Craft::$app->elements->saveElement($entry);
                }
                break;
            default:
                echo "Warning, field type not handled" . $tem->fieldType;
                // var_dump($entry->getFieldValue($fieldLayoutField->handle));
                return;
        }
    }

    private function getElement($targetSite, $entryType, $entryId, $parent)
    {
        $element = null;
        switch ($entryType) {
            case 'craft\\elements\\Entry':
                $element = Entry::find()
                    ->site($targetSite)
                    ->id($entryId)
                    ->one();
                break;
            case 'craft\\elements\\Category':
                $element = Category::find()
                    ->site($targetSite)
                    ->id($entryId)
                    ->one();

                break;
            case 'craft\\elements\\GlobalSet':
                $element = GlobalSet::find()
                    ->site($targetSite)
                    ->id($entryId)
                    ->one();
                break;
            case 'craft\elements\MatrixBlock':
            case 'verbb\supertable\elements\SuperTableBlockElement':
                $parentElement = $this->getElement($targetSite, $parent->entryType, $parent->entryId, $parent->parent);
                if (!is_null($parentElement)) {
                    $blocks = $parentElement->getFieldValue($parent->fieldHandle)->all();
                    foreach ($blocks as $block) {
                        if ($block->id === $entryId) {
                            $element = $block;
                            break;
                        }
                    }
                }
                break;
            case 'craft\\elements\\Asset':
                $element = Asset::find()
                    ->site($targetSite)
                    ->id($entryId)
                    ->one();
                break;
            default:
                Craft::warning("Warning, entry type not handled: " . $entryType, 'field-translate');
                break;
        }

        return $element;
    }

    public function createGlossary()
    {
        $pluginSettings =  Craft::$app->plugins->getPlugin('field-translate')->getSettings();
        $gtKeys = json_decode($pluginSettings->gtKeys, true);
        $translationClient = new TranslationServiceClient(array("credentials" => $gtKeys));

        try {
            $operationResponse = $translationClient->deleteGlossary("projects/" . $gtKeys['project_id'] . "/locations/us-central1/glossaries/" . $pluginSettings->glossaryId);
            $operationResponse->pollUntilComplete();
            if ($operationResponse->operationSucceeded()) {
                $result = $operationResponse->getResult();
                // doSomethingWith($result)
            } else {
                $error = $operationResponse->getError();
                Craft::error($error, 'field-translate');
            }
            $gcsSource = new GcsSource();
            $gcsSource->setInputUri("gs://dko-glossaries/dko_glossar.csv");

            $inputConfig = new GlossaryInputConfig();
            $inputConfig->setGcsSource($gcsSource);

            $langCodes = array("de", "en", "fr", "es", "zh", "sw", "ha", "yo", "xh", "sm");

            $languageCodesSet = new LanguageCodesSet();
            $languageCodesSet->setLanguageCodes($langCodes);

            $glossary = new Glossary();
            $glossary->setInputConfig($inputConfig);
            $glossary->setLanguageCodesSet($languageCodesSet);
            $glossary->setName("projects/" . $gtKeys['project_id'] . "/locations/us-central1/glossaries/" . $pluginSettings->glossaryId);


            $formattedParent = $translationClient->locationName($gtKeys['project_id'], 'us-central1');
            // var_dump($formattedParent);

            $operationResponse = $translationClient->createGlossary($formattedParent, $glossary);
            $operationResponse->pollUntilComplete();
            if ($operationResponse->operationSucceeded()) {
                $result = $operationResponse->getResult();
                // var_dump($result);
                return "success";
            } else {
                $error = $operationResponse->getError();

                Craft::error($error, 'field-translate');
                // handleError($error)
                return "error";
            }
        } finally {
            $translationClient->close();
        }
    }
}
